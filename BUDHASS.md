# Budha WebSS

Budha Inc. is a website development company. They create and publish websites for their clients. At Budha, there are tons of deployed websites, and crazy R&D developers need to see what happens on these websites visually on a regular basis. They are cooking up some crazy AI back in the kitchen, and they need to feed this AI monster with screenshots of the websites.

Now, assume you, as a NodeJS backend developer, come to the R&D team’s aid. They want you to build a backend service that periodically takes a screenshot of a given website address with a given timeout and saves the taken screenshot with metadata. There is no need for authentication and authorization because this is just an internal project, therefore just focus on the main objective.

So that was the story; now, let’s get down to the business. Below is a list of user stories;

- user can create a screenshot record with website address and timeout
- user can delete a screenshot record
- user can pause/unpause a screenshot record
- the system periodically takes screenshots with respect to timeout and saves the images and metadata
- the system ignores paused screenshot records
- the system has to remember where it was left off and continue when it restarts
- user can read all screenshots of a website address
