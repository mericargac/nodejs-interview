# Budha WebSS

Budha Inc. is a website development company. They create and publish websites for their clients. At Budha, there are tons of deployed websites, and crazy R&D developers need to see what happens on these websites visually on a regular basis. They are cooking up some crazy AI back in the kitchen, and they need to feed this AI monster with screenshots of the websites. They require a backend service that speaks REST nad periodically takes a screenshot of a given website address with a given timeout and saves the taken screenshot image and saves it as a PNG file.

There is an application built using NodeJS and Express in order to satisfy the R&D team’s requirements. Main application data is stored in the memory and consist of **Screenshot Configs** and **Screenshot Records**. **Screenshot Config** has URL and timeout information. **Screenshot Configs** are the list of URLs to take periodic screenshots with respect to their timeout values. When a **Screenshot Config** is created it is also registers for the periodic task under the URL value . For every screenshot taken, application saves a PNG image under `screenshot` direcotory and creates a **Screenshot Record** with timestamp, saved PNG filename and URL of the respective **Screenshot Config**. Every **Screenshot Config** has many **Screenshot Records** and **Screenshot Records** are linked to a **Screenshot Config** by URL value. The built application uses REST API for communication and there are four endpoints implemented. `EventEmitter` and `setTimeout` are used for implementing task registry and execution of periodic screenshot tasks.

You can simply test the functionality of the application by using `npm start` to start the project then send `POST` request to `http://localhost:3000/ss/config` address with `{"url": "https://google.com","timeout": 10000}` body for creating **Screenshot Config**. After recieving `{"message": "success"}` application should save a PNG screenshot of the https://google.com website every 10 seconds.

## how to run?

- make sure you are using 12.+ version of NodeJS.
- install dependencies `npm install`.
- run `npm start`.

## how to use?

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/4663bb749905ca577a38)

- GET /ss/config - returns all screenshot configs.
- POST /ss/config - creates a screenshot config for periodic website screenshot saves.

  ```json
  {
    "url": "https://someurl.com",
    "timeout": 10000 // milliseconds
  }
  ```

- GET /ss/record - returns all taken screenshot records.

- GET /image/:imagename - serves saved image by its name.

## Application Structure

![alt appdiagram](app_diagram.png)
