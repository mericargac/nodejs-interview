const ssEventService = require('./service/ssEvent');
const screenshotService = require('./service/screenshot');
const screenshotRecordsData = require('./data/screenshotRecords');

/**
 * take screenshot event handler
 * @param {string} url
 * @param {number} timeout
 */
function handleTakeScreenshot(url, timeout) {
  // QUESTION: what is the limitation of setTimeout?
  // TODO: fix the limitation of setTimeout.
  setTimeout(async () => {
    const filename = await screenshotService.takeScreenShot(url);
    screenshotRecordsData.create({
      url,
      timestamp: Date.now(),
      filename,
    });
    ssEventService.emitTakeScreenshot(url);
  }, timeout);
}

/**
 * initialize and register workers
 */
function init() {
  ssEventService.onTakeScreenshot(handleTakeScreenshot);
}

module.exports = { init };
