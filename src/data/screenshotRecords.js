const DataStore = require('./dataStore');

/**
 * screenshot record instance.
 * @typedef {Object} ScreenshotRecord
 * @property {string} url
 * @property {string} filename
 * @property {number} timestamp
 */

/**
 * store screenshot record data.
 */
class ScreenshotRecords extends DataStore {
  /**
   * read all screenshot records between given date range.
   * @param {Date} from
   * @param {Date} to
   * @return {Array<ScreenshotRecord>}
   */
  readByDateRange(from, to) {
    return this.records.filter(
      ({ timestamp }) =>
        from.valueOf() >= timestamp && to.valueOf() <= timestamp
    );
  }

  /**
   * filters records by given url
   * @param {string} url
   * @return {Array<ScreenshotRecord>}
   */
  readByUrl(url) {
    return this.records.filter(value => value.url === url);
  }
}

module.exports = new ScreenshotRecords();
