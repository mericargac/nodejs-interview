const DataStore = require('./dataStore');

/**
 * screenshot config for taking periodic screenshots.
 * @typedef {Object} ScreenshotConfig
 * @property {string} url
 * @property {number} timeout
 */

/**
 * store screenshot config data.
 */
class ScreenshotConfigs extends DataStore {}

module.exports = new ScreenshotConfigs();
