/**
 * generic in memory data storage
 */
class DataStore {
  /**
   * initialize record array.
   */
  constructor() {
    this.records = [];
  }
  /**
   * create a record
   * @param {*} newRecord
   */
  create(newRecord) {
    this.records.push(newRecord);
  }
  /**
   * read records
   * @return {Array<*>}
   */
  read() {
    return this.records;
  }
  /**
   * update a record
   */
  update() {
    // TODO
  }
  /**
   * delete a record
   */
  delete() {
    // TODO
  }
}

// QUESTION: Is using memory for storage a good idea especially for this project, why?
// QUESTION: How to persist the data?
// TODO: what to use as a storage, why it is better and how to scale it.
module.exports = DataStore;
