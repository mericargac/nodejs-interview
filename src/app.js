const express = require('express');

const api = require('./api');
const worker = require('./worker');
const screenshotService = require('./service/screenshot');
/**
 * HTTP server setup and register API.
 */
const app = express();
// QUESTION: why do we use this line of code?
app.use(express.json());
app.use('/ss', api);
// register static file server.
app.use('/image', express.static(screenshotService.BASE_DIR));
// TODO: add centeralized error handler.
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`budhass is up and running on ${port}`);
});

/**
 * start background workers.
 */
worker.init();
