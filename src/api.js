const { Router } = require('express');

const screenshotConfigs = require('./data/screenshotConfigs');
const screenshotRecords = require('./data/screenshotRecords');
const ssEventService = require('./service/ssEvent');

const router = new Router();

/**
 * screenshot configuration rest api
 */
router.get('/config', (req, res) => {
  const data = screenshotConfigs.read();
  res.json(data);
});
router.post('/config', (req, res) => {
  // TODO: add input validation
  const newSSConfig = req.body;
  screenshotConfigs.create(newSSConfig);
  // TODO: add error handler might throw error
  ssEventService.registerScreenshot(
    newSSConfig.url,
    newSSConfig.timeout
  );
  res.json({ message: 'success' });
});

// TODO: update endpoint for registered screenshot configs with REST API standarts

// TODO: delete endpoint for registered screenshot configs with REST API standarts

/**
 * screenshot records rest api
 */
router.get('/record', (req, res) => {
  // TODO: add query params for createdAt field in the record
  // TODO: add id field to screenshot config and always read records by config id.
  const records = screenshotRecords.read();
  res.json(records);
});

module.exports = router;
