const events = require('events');

const EVENT_KEYS = {
  TAKE_SCREENSHOT: 'TAKE_SCREENSHOT',
};

const ssEventEmitter = new events.EventEmitter();

/**
 * store registered urls with timeouts
 */
const registeredUrls = {};

/**
 * emits take screenshot for registered urls.
 * @param {string} url
 */
function emitTakeScreenshot(url) {
  if (!registeredUrls[url]) {
    throw new Error('url is not registered to system.');
  }
  ssEventEmitter.emit(
    EVENT_KEYS.TAKE_SCREENSHOT,
    JSON.stringify({ url, timeout: registeredUrls[url] })
  );
}

/**
 * registers a screenshot config to the system.
 * @param {string} url
 * @param {number} timeout
 */
function registerScreenshot(url, timeout) {
  if (registeredUrls[url]) {
    throw new Error('url is already registered to event system.');
  }
  registeredUrls[url] = timeout;
  emitTakeScreenshot(url);
}

/**
 * this callback is called when a screenshot triggered.
 * @callback takeScreenshotListener
 * @param {string} url
 * @param {number} timeout
 */

/**
 * subscribe to take screenshot events
 * @param {takeScreenshotListener} listener
 */
function onTakeScreenshot(listener) {
  ssEventEmitter.addListener(EVENT_KEYS.TAKE_SCREENSHOT, data => {
    const { url, timeout } = JSON.parse(data);
    listener(url, timeout);
  });
}

// QUESTION: event system is not scalable, why and what to use instead?
// QUESTION: what is the scale strategy?
module.exports = {
  registerScreenshot,
  onTakeScreenshot,
  emitTakeScreenshot,
};
