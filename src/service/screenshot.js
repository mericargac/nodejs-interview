const path = require('path');
const fs = require('fs');

const captureWebsite = require('capture-website');

const BASE_DIR = path.join(process.cwd(), 'screenshots');

// check if the base directory for images exists.
if (!fs.existsSync(BASE_DIR)) {
  // if it does not exists create the directory
  fs.mkdirSync(BASE_DIR);
}

// QUESTION: why did we sanitize here?
/**
 * sanitize given string name value.
 * @param {string} text
 * @return {string}
 */
function sanitizeName(text) {
  return text.replace(/[^a-z0-9áéíóúñü \.,_-]/gim, '').trim();
}

/**
 * take a screenshot and save to file.
 * @param {string} url
 * @param {string} filename
 * @return {Promise<string>}
 */
async function takeScreenShot(url) {
  const now = new Date();
  const recordName = sanitizeName(`${url}_${now.toUTCString()}.png`);
  const filePath = path.join(BASE_DIR, recordName);
  await captureWebsite.file(url, filePath);
  return recordName;
}

// TODO: save the image without sanitizing.
// QUESTION: saving to file system is not scalable, what to use instead.
// QUESTION: what is the scale strategy.
module.exports = { BASE_DIR, takeScreenShot };
