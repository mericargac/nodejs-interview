# Interview Sheet

## Main Tasks

1. add DataStore class an id mechanism for storing records with a unique identifier.
2. add validation for POST /ss/config endpoint when creating screenshot config.
3. create a GET /ss/config/:id endpoint for retrieving a screenshot config by id.
4. in ssEvent service screenshot config timeouts are registered under url change the url key to id.
5. add update, delete operation for screenshot config REST API.
6. find and fix setTimeout problem.
7. save images without the need of sanitizing.

## Extra Tasks

8. persist application data.
9. pause and unpause registered periodic screenshot config actions.
